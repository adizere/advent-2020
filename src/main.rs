mod formula;

use crate::formula::*;

fn main() {
    let input = get_input();

    for x in input {
        let f = Formula::from_string(x);
        println!("Computing the formula {:?}", f);
        println!("Result={:?}", f.result());
    }
}

fn get_input() -> Vec<String> {
    let all = vec!["((2 + 4 * 9) * (6 + 9 * 8 + 6) + 6) + 2 + 4 * 2".to_string()];

    all
}

#[cfg(test)]
mod tests {
    use crate::formula::Formula;

    #[test]
    fn inputs_outputs() {
        struct Test {
            input: String,
            output: u32,
            expect_pass: bool,
        };

        let tests: Vec<Test> = vec![
            Test {
                input: "1 + 2 * 3 + 4 * 5 + 6".to_string(),
                output: 71,
                expect_pass: true,
            },
            Test {
                input: "1 + (2 * 3) + (4 * (5 + 6))".to_string(),
                output: 51,
                expect_pass: true,
            },
            Test {
                input: "2 * 3 + (4 * 5)".to_string(),
                output: 26,
                expect_pass: true,
            },
            Test {
                input: "5 + (8 * 3 + 9 + 3 * 4 * 3)".to_string(),
                output: 437,
                expect_pass: true,
            },
            Test {
                input: "5 * 9 * (7 * 3 * 3 + 9 * 3 + (8 + 6 * 4))".to_string(),
                output: 12240,
                expect_pass: true,
            },
            Test {
                input: "9 * (7 * 3 * 3 + 9 * 3 + (8 + 6 * 4))".to_string(),
                output: 12240,
                expect_pass: false,
            },
            Test {
                input: "((2 + 4 * 9) * (6 + 9 * 8 + 6) + 6) + 2 + 4 * 2".to_string(),
                output: 13632,
                expect_pass: true,
            },
        ];

        for t in tests {
            let f = Formula::from_string(t.input.clone());
            let o = f.result();
            if t.expect_pass {
                assert_eq!(o, t.output, "Test failed for input: {:?}", t.input);
            } else {
                assert_ne!(
                    o, t.output,
                    "Test was supposed to fail for input: {:?}",
                    t.input
                );
            }
        }
    }
}
