use std::fmt;

/// Operators
#[derive(Debug, Clone)]
pub enum Operator {
    Add,
    Multiply,
}

impl Operator {
    pub fn apply(&self, lhs: u32, rhs: u32) -> u32 {
        match self {
            Operator::Add => lhs + rhs,
            Operator::Multiply => lhs * rhs,
        }
    }
}

/// A single element in a formula: either a number, an `Operator`, or a nested `Formula`
#[derive(Clone)]
pub enum Element {
    Op(Operator),
    Number(u32),
    Nested(Formula),
}

impl fmt::Debug for Element {
    fn fmt(&self, f: &mut fmt::Formatter<'_>) -> fmt::Result {
        match self {
            Element::Op(op) => {
                write!(f, "{:?}", op)
            }
            Element::Number(nr) => {
                write!(f, "{:?}", nr)
            }
            Element::Nested(fr) => {
                write!(f, "\n\tF[{:?}]", fr)
            }
        }
    }
}

/// A formula is a an expression consisting of numbers and `Operator`s (parentheses are stripped).
/// Formulas can be nested inside one another.
#[derive(Debug, Clone)]
pub struct Formula {
    pub list: Vec<Element>,
}

impl Formula {
    pub fn new() -> Self {
        Formula { list: vec![] }
    }

    pub fn empty(&self) -> bool {
        self.list.is_empty()
    }

    pub fn result(&self) -> u32 {
        let mut res = 0;
        let mut op_stack: Vec<Operator> = vec![];
        let iterator = self.list.iter();

        for token in iterator {
            println!("Handling token: {:?}", token.clone());
            match token {
                Element::Op(o) => op_stack.push(o.clone()),
                Element::Number(n) => {
                    if let Some(op) = op_stack.pop() {
                        res = op.apply(res, *n);
                    } else {
                        res = *n
                    }
                }
                Element::Nested(form) => {
                    if let Some(op) = op_stack.pop() {
                        res = op.apply(res, form.result());
                    } else {
                        res = form.result()
                    }
                }
            }
        }

        res
    }

    /// Parses a String into a `Formula`
    pub fn from_string(inp: String) -> Formula {
        println!("Parsing String {:?}", inp);

        // Formulas are separated by '('
        let formulas_raw: Vec<String> = inp.split_terminator('(').map(|s| s.to_string()).collect();

        // Pre-parse in a basic intermediary notation
        let mut interim: Vec<String> = vec![];
        for raw_part in &formulas_raw {
            println!("Token {:?}", raw_part);

            // Separate formulas from each other by splitting off end parentheses
            let mut res = separate_endings(raw_part.clone());

            interim.append(&mut res);

            println!("Checking the output tokens: {:?}\n\n", interim);
        }

        // Parse into a Formula
        let mut output = Formula::new();
        Formula::parse_all(&mut output, interim);

        println!("parsed into: {:?}", output);

        output
    }

    fn parse_all(into: &mut Formula, interim_repres: Vec<String>) -> Option<Vec<String>> {
        let mut inp = interim_repres.iter();

        println!(
            "\t\t|| Parsing start for: {:?} into: {:?}",
            interim_repres, into
        );

        // Take the first element, parse, and call recursively
        if let Some(token) = inp.next() {
            let id = token.chars().next().unwrap_or(' ');

            return if token.eq(")") {
                println!("\t\t|{:?}| Stopping", id);
                // Return anything that is left un-parsed, the higher-level formula will
                // include it.
                if inp.clone().peekable().peek().is_some() {
                    Some(inp.cloned().collect())
                } else {
                    None
                }
            } else {
                println!("\t\t|{:?}| A formula stars here", id);

                // A formula stars here, parse it
                let mut elems = Formula::parse_elements(token.clone());
                into.list.append(&mut elems);

                // Now handle whatever is left, recursively
                let inner_formula_raw = inp.cloned().collect();
                println!("\t\t|{:?}| Inner (suffix?)={:?}", id, inner_formula_raw);
                let mut nested = Formula::new();
                let cont = Formula::parse_all(&mut nested, inner_formula_raw);

                println!("\t\t|{:?}| Leftover={:?}. Nested={:?}", id, cont, nested);

                // There is still some string(s) lefts to parse, either as continuation to the
                // current formula (if the nested one was non-empty), or otherwise as
                // continuation to the upper-level formula (return the leftover).
                if let Some(leftover) = cont.clone() {
                    println!(
                        "\t\t|{:?}| Continue parsing: {:?} /  {:?}",
                        id, cont, nested
                    );

                    if !nested.empty() {
                        // There was a nested formula, include it in our current one
                        into.list.push(Element::Nested(nested));
                        // Continue parsing..
                        Formula::parse_all(into, leftover)
                    } else {
                        Some(leftover)
                    }
                } else {
                    None
                }
            };
        }
        None
    }

    /// Given an uninterrupted string of numbers and operators, returns the list of elements
    fn parse_elements(s: String) -> Vec<Element> {
        let mut res = vec![];

        let tokens = s.split_whitespace();

        for tok in tokens {
            if let Some(number) = parse_number(tok) {
                res.push(Element::Number(number))
            } else if let Some(op) = parse_operator(tok) {
                res.push(Element::Op(op));
            }
        }

        println!(
            "\t\t\t** interim={:?}\n\t\t\t  -> parsed sub-formula={:?}",
            s, res
        );

        res
    }
}

fn separate_endings(mut given: String) -> Vec<String> {
    println!("===== parse_single for {:?}", given);

    let mut output_tokens: Vec<String> = vec![];

    while let Some(idx) = given.find(')') {
        println!(" \t\tFound on {:?}", idx);

        let mut p = given.clone();
        let s = if idx == 0 {
            p.split_off(idx + 1)
        } else {
            p.split_off(idx)
        };

        println!("\t\tsplit into: {:?} and {:?}", p, s);

        output_tokens.push(p);
        given = s;

        println!("\t\tChecking interim {:?}\n\n", output_tokens);
    }

    output_tokens.push(given);
    println!(" ==== FINISHED parse_single with {:?}", output_tokens);

    output_tokens
}

fn parse_operator(input: &str) -> Option<Operator> {
    match input {
        "+" => Some(Operator::Add),
        "*" => Some(Operator::Multiply),
        _ => None,
    }
}

fn parse_number(input: &str) -> Option<u32> {
    let input_opt = input.parse::<u32>();

    input_opt.ok()
}
